/*
2021 © Postgres.ai
*/

// Package networks describes custom network elements.
package networks

const (
	// DLEApp contains name of the Database Lab Engine network.
	DLEApp = "DLE"

	// InternalType contains name of the internal network type.
	InternalType = "internal"
)
